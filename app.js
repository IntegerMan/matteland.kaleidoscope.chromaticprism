let express = require('express');
let logger = require('morgan');
let nlp = require('compromise');

//var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');

//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

function rawBody(req, res, next) {
  req.setEncoding('utf8');
  req.rawBody = '';
  req.on('data', function(chunk) {
    req.rawBody += chunk;
  });
  req.on('end', function(){
    next();
  });
}

// Initialize the app
let app = express();
app.use(logger('dev'));
app.use(rawBody);
//app.use(express.json());

// Setup server port
const port = process.env.PORT || 1337;

let parseText = function (input) {
  console.log('Parsing text ' + input);

  const parsed = nlp(input);
  parsed.debug();

  return parsed;
};

app.get('/', (req, res) => {
  let parsed = parseText('Send a POST request to get data');
  return res.send(parsed.terms().data());
});

app.post('/', (req, res) => {
  let parsed = parseText(req.rawBody);
  return res.send(parsed.terms().data());
});

app.post('/terms', (req, res) => {
  let parsed = parseText(req.rawBody);
  return res.send(parsed.terms().data());
});

// Launch app to listen to specified port
app.listen(port, function () {
  console.log("Chromatic Prism running on port " + port);
});

module.exports = app;
